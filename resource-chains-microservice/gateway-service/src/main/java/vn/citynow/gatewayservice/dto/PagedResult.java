package vn.citynow.gatewayservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
@AllArgsConstructor
public class PagedResult<T> {

    private Collection<T> contents;
    private long totalElements;
    private int totalPages;
    private int size;
    private int page;
    private boolean isFirst;
    private boolean isLast;

}
