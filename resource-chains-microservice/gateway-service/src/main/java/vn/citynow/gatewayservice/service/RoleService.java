package vn.citynow.gatewayservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import vn.citynow.gatewayservice.model.Role;
import vn.citynow.gatewayservice.repository.RoleRepository;

import java.util.List;

@Service
@Slf4j
public class RoleService {

    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public List<Role> getAll() {
        log.info("Get all role codes");
        return roleRepository.findAll();
    }

    int getDisplayFlag() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN")))
            return 1;
        if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("MANAGER")))
            return 2;
        return 3;
    }

}
