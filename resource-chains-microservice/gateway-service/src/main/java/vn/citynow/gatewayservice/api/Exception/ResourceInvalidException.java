package vn.citynow.gatewayservice.api.Exception;

public class ResourceInvalidException extends RuntimeException {

    public ResourceInvalidException() {
        super();
    }

    public ResourceInvalidException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceInvalidException(String message) {
        super(message);
    }

    public ResourceInvalidException(Throwable cause) {
        super(cause);
    }

}
