package vn.citynow.gatewayservice.dto.User;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.citynow.gatewayservice.dto.StatusDto;

import java.time.Instant;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Long id;

    private String username;

    private String email;

    private String roleCode;

    private String avatar;

    private String firstName;

    private String lastName;

    private String nameHiragana;

    private String address;

    private String phoneNumber;

    private boolean delFlag;

    private Instant createdAt;

    private Instant updatedAt;

    private String createdBy;

    private String updatedBy;

    private String permission;

    private String frontOfCard;

    private String backOfCard;

    private Date dateOfExpiration;

    private StatusDto status;

}
