package vn.citynow.gatewayservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.citynow.gatewayservice.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long>, JpaSpecificationExecutor<User> {

    @Transactional(readOnly = true)
    Optional<User> findByDelFlagFalseAndUsername(String username);

    @Transactional(readOnly = true)
    boolean existsByUsername(String username);

    @Transactional(readOnly = true)
    List<User> findAllByDelFlagFalse();

    @Transactional(readOnly = true)
    Optional<User> findByDelFlagFalseAndId(Long id);

    @Modifying
    @Query("update User u set u.delFlag = true where u.id = ?1")
    int deleteUser(Long id);

    @Modifying
    @Query("update User u set u.status = null where u.status.id = ?1")
    int modifyUserStatus(Long id);

    @Transactional(readOnly = true)
    boolean existsByDelFlagFalseAndUsernameAndRoleCode(String username, String role_code);

    @Modifying
    @Query("update User u set u.openProjectId = ?2 where u.id = ?1")
    int modifyUserOpenProjectId(Long id, Long openProjectId);

}
