package vn.citynow.gatewayservice.constant;

public class RoleCode {

    public static final String SUPER_ADMIN = "SUPER_ADMIN";
    public static final String ADMIN = "ADMIN";
    public static final String MANAGER = "MANAGER";
    public static final String NORMAL_USER = "NORMAL_USER";

}
