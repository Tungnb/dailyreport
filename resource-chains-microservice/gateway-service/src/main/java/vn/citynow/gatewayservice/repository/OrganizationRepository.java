package vn.citynow.gatewayservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.citynow.gatewayservice.model.Organization;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, String> {

    List<Organization> findAllByDelFlagFalse();

    Optional<Organization> findByDelFlagFalseAndCode(String code);

    @Transactional(readOnly = false)
    boolean existsByDelFlagFalseAndCode(String code);

    @Transactional(readOnly = false)
    boolean existsByDelFlagFalseAndName(String name);

}
