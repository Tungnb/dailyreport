package vn.citynow.gatewayservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.citynow.gatewayservice.model.Status;

import java.util.List;
import java.util.Optional;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {

    @Transactional(readOnly = true)
    List<Status> findAllByDelFlagFalseAndOrganizationCode(String organization_code);

    @Transactional(readOnly = true)
    Optional<Status> findByDelFlagFalseAndIdAndOrganizationCode(Long id, String organization_code);

    @Modifying
    @Query("update Status s set s.delFlag = true where s.id = ?1 and s.organization.code = ?2")
    int deleteStatus(Long id, String organization);

}
