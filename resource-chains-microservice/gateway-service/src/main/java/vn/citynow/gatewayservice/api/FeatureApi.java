package vn.citynow.gatewayservice.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.citynow.gatewayservice.dto.ApiResponse;

import java.util.List;

import static vn.citynow.gatewayservice.constant.DefaultUserBranchOrganization.ORGANIZATION;



@RestController
@RequestMapping(value = "/api/features")
public class FeatureApi {

    @Value("${features}")
    private String[] features;

    @GetMapping({"/all"})
    public ApiResponse getByToken(@RequestParam(value = "organization", defaultValue = ORGANIZATION) String organization) {
        return new ApiResponse(
                HttpStatus.OK,
                "Get all features by token successfully",
                features
        );
    }
}
