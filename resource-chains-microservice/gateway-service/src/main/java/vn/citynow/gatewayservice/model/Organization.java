package vn.citynow.gatewayservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "organization", schema = "platform")
@Getter
@Setter
public class Organization {

    @Id
    @Column(name = "code", length = 100)
    private String code;

    private String name;

    private boolean delFlag;

    @Column(nullable = false, updatable = false)
    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

    @CreatedBy
    private String createdBy;

    @LastModifiedBy
    private String updatedBy;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "organization", orphanRemoval = true)
    private List<Branch> branches;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "organization", orphanRemoval = true)
    private List<UserBranchOrganization> userBranchOrganizations;


    public Organization code(String code) {
        this.code = code;
        return this;
    }

    public Organization name(String name) {
        this.name = name;
        return this;
    }

}
