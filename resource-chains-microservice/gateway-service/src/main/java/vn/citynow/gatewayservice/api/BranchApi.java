package vn.citynow.gatewayservice.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vn.citynow.gatewayservice.dto.ApiResponse;
import vn.citynow.gatewayservice.dto.mapper.BranchMapper;
import vn.citynow.gatewayservice.service.BranchService;

import static vn.citynow.gatewayservice.constant.DefaultUserBranchOrganization.ORGANIZATION;


@RestController
@RequestMapping(value = "/api/branches")
public class BranchApi {

    private final BranchService branchService;
    private final BranchMapper branchMapper;

    public BranchApi(BranchService branchService, BranchMapper branchMapper) {
        this.branchService = branchService;
        this.branchMapper = branchMapper;
    }

    @GetMapping({"/token"})
    public ApiResponse getByToken(@RequestParam(value = "organization", defaultValue = ORGANIZATION) String organization) {
        return new ApiResponse(
                HttpStatus.OK,
                "Get all branches by token successfully",
                branchMapper.toBranchDtoList(branchService.getByToken(organization))
        );
    }

}
