package vn.citynow.gatewayservice.model;

import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "privilege")
@Table(schema="platform")
@NoArgsConstructor
public class Privilege implements GrantedAuthority {

    @Id
    @Column(name = "code",length = 100)
    private String code;

    @ManyToMany(mappedBy = "privileges")
    private List<Role> roles = new ArrayList<>();

    @Column(name = "name")
    private String name;

    @Override
    public String toString() {
        return this.code;
    }

    @Override
    public String getAuthority() {
        return this.code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Privilege){
            return this.code.equals(((Privilege) o).code);
        }
        return false;
    }

    public Privilege(String code) {
        this.code = code;
    }

}
