package vn.citynow.gatewayservice.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.citynow.gatewayservice.dto.StatusDto;
import vn.citynow.gatewayservice.model.Status;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StatusMapper {

    @Mapping(target = "organization", ignore = true)
    @Mapping(target = "updatedBy", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "delFlag", ignore = true)
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "id", ignore = true)
    Status toStatus(StatusDto statusDto);

    StatusDto toStatusDto(Status status);

    List<StatusDto> toStatusDtoList(List<Status> statusList);

}
