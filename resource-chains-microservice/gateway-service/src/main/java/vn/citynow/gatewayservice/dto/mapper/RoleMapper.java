package vn.citynow.gatewayservice.dto.mapper;

import org.mapstruct.Mapper;
import vn.citynow.gatewayservice.dto.Role.RoleCodeDto;
import vn.citynow.gatewayservice.model.Role;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    RoleCodeDto toRoleCodeDto(Role role);

    List<RoleCodeDto> toRoleCodeDtoList(List<Role> roleList);

}
