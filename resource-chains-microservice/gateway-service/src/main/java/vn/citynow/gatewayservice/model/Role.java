package vn.citynow.gatewayservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity(name = "role")
@Table(schema="platform")
@Getter
@Setter
public class Role implements GrantedAuthority {

    @Id
    @Column(name = "code", length = 100)
    private String code;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "role", orphanRemoval = true)
    private List<User> user;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "role", orphanRemoval = true)
    private List<UserBranchOrganization> userBranchOrganizations;

    @Column(name = "name")
    private String name;

    @Column(name = "display_flag")
    private int displayFlag;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    }, fetch = FetchType.EAGER)
    @JoinTable(name = "role_privilege",
            joinColumns = @JoinColumn(name = "role_code"),
            inverseJoinColumns = @JoinColumn(name = "privilege_code")
    )
    private Set<Privilege> privileges = new HashSet<>();

    @Override
    public String getAuthority() {
        return this.code;
    }

}
