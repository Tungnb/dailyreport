package vn.citynow.gatewayservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import vn.citynow.gatewayservice.api.Exception.UnauthorizedException;
import vn.citynow.gatewayservice.model.Privilege;
import vn.citynow.gatewayservice.model.UserBranchOrganization;
import vn.citynow.gatewayservice.repository.UserBranchOrganizationRepository;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AuthorityService {

    private final UserBranchOrganizationRepository userBranchOrganizationRepository;

    public AuthorityService(UserBranchOrganizationRepository userBranchOrganizationRepository) {
        this.userBranchOrganizationRepository = userBranchOrganizationRepository;
    }

    public Set<String> getByToken(String branch, String organization) {
        log.info("Get all role and authorities by token with branch " + branch + " and organization " + organization);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserBranchOrganization userBranchOrganization = userBranchOrganizationRepository
                .findByDelFlagFalseAndUserUsernameAndBranchCodeAndOrganizationCode(authentication.getName(), branch, organization)
                .orElseThrow(() -> new UnauthorizedException("No permission"));
        Set<String> roleAndAuthorities = new HashSet<>();
        roleAndAuthorities.add(userBranchOrganization.getRole().getCode());
        roleAndAuthorities.addAll(userBranchOrganization.getRole().getPrivileges().stream().map(Privilege::getCode).collect(Collectors.toSet()));
        return roleAndAuthorities;
    }

}
