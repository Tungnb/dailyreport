package vn.citynow.gatewayservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.Set;

@Entity
@Table(name = "branch", schema = "platform")
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Branch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String name;

    private boolean delFlag;

    @Column(nullable = false, updatable = false)
    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

    @CreatedBy
    private String createdBy;

    @LastModifiedBy
    private String updatedBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_code")
    private Organization organization;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "branch", orphanRemoval = true)
    private Set<UserBranchOrganization> userBranchOrganizations;

    public Branch code(String code) {
        this.code = code;
        return this;
    }

    public Branch name(String name) {
        this.name = name;
        return this;
    }

    public Branch organization(Organization organization) {
        this.organization = organization;
        return this;
    }


}
