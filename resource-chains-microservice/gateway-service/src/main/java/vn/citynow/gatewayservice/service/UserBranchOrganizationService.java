package vn.citynow.gatewayservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.citynow.gatewayservice.api.Exception.ResourceInvalidException;
import vn.citynow.gatewayservice.api.Exception.ResourceNotFoundException;
import vn.citynow.gatewayservice.api.Exception.UnauthorizedException;
import vn.citynow.gatewayservice.dto.UserBranchRoleDto;
import vn.citynow.gatewayservice.model.*;
import vn.citynow.gatewayservice.repository.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static vn.citynow.gatewayservice.constant.RoleCode.ADMIN;
import static vn.citynow.gatewayservice.constant.RoleCode.NORMAL_USER;

@Service
@Slf4j
public class UserBranchOrganizationService {

    private final UserBranchOrganizationRepository userBranchOrganizationRepository;
    private final UserRepository userRepository;
    private final BranchRepository branchRepository;
    private final OrganizationRepository organizationRepository;
    private final RoleRepository roleRepository;

    public UserBranchOrganizationService(UserBranchOrganizationRepository userBranchOrganizationRepository, UserRepository userRepository, BranchRepository branchRepository, OrganizationRepository organizationRepository, RoleRepository roleRepository) {
        this.userBranchOrganizationRepository = userBranchOrganizationRepository;
        this.userRepository = userRepository;
        this.branchRepository = branchRepository;
        this.organizationRepository = organizationRepository;
        this.roleRepository = roleRepository;
    }

    void checkPermission(String organization, String authority) {
        log.info("Check if user"
                + " in organization " + organization
                + " and has authority " + authority);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Set<String> privilegeSet = userBranchOrganizationRepository
                .findAllByDelFlagFalseAndUserUsernameAndOrganizationCode(authentication.getName(), organization)
                .stream()
                .map(UserBranchOrganization::getRole)
                .distinct()
                .map(Role::getPrivileges)
                .flatMap(Set::stream)
                .map(Privilege::getCode)
                .collect(Collectors.toSet());
        if (!privilegeSet.contains(authority)) {
            throw new UnauthorizedException("No permission");
        }
    }

    User checkPermissionAndGetUser(String branch, String organization, String authority) {
        log.info("Check if user"
                + " in branch " + branch + " and organization " + organization
                + " and has authority " + authority);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserBranchOrganization userBranchOrganization = userBranchOrganizationRepository
                .findByDelFlagFalseAndUserUsernameAndBranchCodeAndOrganizationCode(authentication.getName(), branch, organization)
                .orElseThrow(() -> new UnauthorizedException("No permission"));
        if (!userBranchOrganization.getRole().getPrivileges().stream().map(Privilege::getCode).collect(Collectors.toSet()).contains(authority)) {
            throw new UnauthorizedException("No permission");
        }
        return userBranchOrganization.getUser();
    }

    UserBranchOrganization checkPermissionAndGetUserBranchOrganization(String branch, String organization, String authority) {
        log.info("Check if user"
                + " in branch " + branch + " and organization " + organization
                + " and has authority " + authority);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserBranchOrganization userBranchOrganization = userBranchOrganizationRepository
                .findByDelFlagFalseAndUserUsernameAndBranchCodeAndOrganizationCode(authentication.getName(), branch, organization)
                .orElseThrow(() -> new UnauthorizedException("No permission"));
        if (!userBranchOrganization.getRole().getPrivileges().stream().map(Privilege::getCode).collect(Collectors.toSet()).contains(authority)) {
            throw new UnauthorizedException("No permission");
        }
        return userBranchOrganization.user(userRepository
                .findByDelFlagFalseAndUsername(authentication.getName())
                .orElseThrow(() -> new ResourceNotFoundException("Cannot find User")));
    }

    UserBranchOrganization getUserBranchOrganization(Long userId, String branch, String organization) {
        log.info("Check if user"
                + " in branch " + branch + " and organization " + organization );
        return userBranchOrganizationRepository
                .findByDelFlagFalseAndUserIdAndBranchCodeAndOrganizationCode(userId, branch, organization)
                .orElseThrow(() -> new UnauthorizedException("No permission"))
                .user(userRepository
                        .findByDelFlagFalseAndId(userId)
                        .orElseThrow(() -> new ResourceNotFoundException("Cannot find User")));
    }

    @Transactional
    List<Branch> getBranch(String organization) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info("Get all branch of user " + authentication.getName() + " in organization " + organization);
        return userBranchOrganizationRepository
                .findAllByDelFlagFalseAndUserUsernameAndOrganizationCode(authentication.getName(), organization)
                .stream()
                .map(UserBranchOrganization::getBranch)
                .collect(Collectors.toList());
    }

    boolean deleteBranch(Long id, String organization) {
        if (branchRepository.deleteBranch(id, organization) == 1)
            return userBranchOrganizationRepository.deleteUserBranchOrganization(id, organization) >= 0;
        return false;
    }

    boolean saveUserBranchOrganization(UserBranchRoleDto userBranchRoleDto, String organization) {
        log.info("Check if branch " + userBranchRoleDto.getBranchId() + " is in organization " + organization);
        if (!branchRepository.existsByDelFlagFalseAndIdAndOrganizationCode(userBranchRoleDto.getBranchId(), organization))
            throw new ResourceInvalidException("Cannot find branch");
        log.info("Check if user with id " + userBranchRoleDto.getUserId() + " is already in branch " + userBranchRoleDto.getBranchId());
        if (userBranchOrganizationRepository.existsByDelFlagFalseAndUserIdAndBranchIdAndOrganizationCode(userBranchRoleDto.getUserId(), userBranchRoleDto.getBranchId(), organization))
            throw new ResourceInvalidException("User is already in branch");
        log.info("Validate role");
        if (userBranchRoleDto.getRole() == null)
            userBranchRoleDto.setRole(NORMAL_USER);
        if (userBranchRoleDto.getRole().equalsIgnoreCase(ADMIN))
            throw new ResourceInvalidException("Role is invalid");
        return userBranchOrganizationRepository.save(new UserBranchOrganization()
                .user(userRepository
                        .findByDelFlagFalseAndId(userBranchRoleDto.getUserId())
                        .orElseThrow(() -> new ResourceNotFoundException("Cannot find user")))
                .role(roleRepository
                        .findById(userBranchRoleDto.getRole())
                        .orElseThrow(() -> new ResourceNotFoundException("Cannot find role")))
                .branch(branchRepository
                        .findByDelFlagFalseAndId(userBranchRoleDto.getBranchId())
                        .orElseThrow(() -> new ResourceNotFoundException("Cannot find branch")))
                .organization(organizationRepository
                        .findByDelFlagFalseAndCode(organization)
                        .orElseThrow(() -> new ResourceNotFoundException("Cannot find organization")))
        ).getId() != null;
    }

    boolean removeUserBranchOrganization(UserBranchRoleDto userBranchRoleDto, String organization) {
        log.info("Check if branch " + userBranchRoleDto.getBranchId() + " is in organization " + organization);
        if (!branchRepository.existsByDelFlagFalseAndIdAndOrganizationCode(userBranchRoleDto.getBranchId(), organization))
            throw new ResourceInvalidException("Cannot find branch");
        log.info("Check if user with id " + userBranchRoleDto.getUserId() + " is already in branch " + userBranchRoleDto.getBranchId());
        UserBranchOrganization userBranchOrganization = userBranchOrganizationRepository
                .findByDelFlagFalseAndUserIdAndBranchIdAndOrganizationCode(userBranchRoleDto.getUserId(), userBranchRoleDto.getBranchId(), organization)
                .orElseThrow(() -> new ResourceInvalidException("User is not in branch"));
        log.info("Validate role");
        if (userBranchOrganization.getRole().getCode().equalsIgnoreCase(ADMIN))
            throw new ResourceInvalidException("Role is invalid");
        return userBranchOrganizationRepository.save(userBranchOrganization.delete()).isDelFlag();
    }

}
