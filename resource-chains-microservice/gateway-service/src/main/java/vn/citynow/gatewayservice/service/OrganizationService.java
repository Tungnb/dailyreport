package vn.citynow.gatewayservice.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import vn.citynow.gatewayservice.api.Exception.UnauthorizedException;
import vn.citynow.gatewayservice.model.Organization;
import vn.citynow.gatewayservice.repository.BranchRepository;
import vn.citynow.gatewayservice.repository.OrganizationRepository;
import vn.citynow.gatewayservice.repository.RoleRepository;
import vn.citynow.gatewayservice.repository.UserRepository;

import java.util.List;

import static vn.citynow.gatewayservice.constant.RoleCode.SUPER_ADMIN;


@Service
@Slf4j
public class OrganizationService {

    private final OrganizationRepository organizationRepository;
    private final BranchRepository branchRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public OrganizationService(OrganizationRepository organizationRepository, BranchRepository branchRepository, UserRepository userRepository, RoleRepository roleRepository) {
        this.organizationRepository = organizationRepository;
        this.branchRepository = branchRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    public List<Organization> getAll() {
        log.info("Get all organization");

        log.info("Check permission");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getAuthorities().stream().noneMatch(a -> a.getAuthority().equals(SUPER_ADMIN)))
            throw new UnauthorizedException("No permission");

        return organizationRepository.findAllByDelFlagFalse();
    }
}
