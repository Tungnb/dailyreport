package vn.citynow.gatewayservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.citynow.gatewayservice.model.Branch;

import java.util.List;
import java.util.Optional;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Long> {

    List<Branch> findAllByDelFlagFalse();

    @Transactional(readOnly = true)
    Optional<Branch> findByDelFlagFalseAndId(Long id);

    @Transactional(readOnly = true)
    Optional<Branch> findByDelFlagFalseAndCodeAndOrganizationCode(String code, String organization_code);

    List<Branch> findAllByDelFlagFalseAndOrganizationCode(String organization_code);

    @Transactional(readOnly = true)
    boolean existsByDelFlagFalseAndIdAndOrganizationCode(Long id, String organization_code);

    @Transactional(readOnly = true)
    boolean existsByDelFlagFalseAndCodeAndOrganizationCode(String code, String organization_code);

    @Transactional(readOnly = true)
    boolean existsByDelFlagFalseAndNameAndOrganizationCode(String name, String organization_code);

    @Transactional
    @Modifying
    @Query("update Branch b set b.delFlag = true where b.id = ?1 and b.organization.code = ?2")
    int deleteBranch(Long id, String organization);

}
