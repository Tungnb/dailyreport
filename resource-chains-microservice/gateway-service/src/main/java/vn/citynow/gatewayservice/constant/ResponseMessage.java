package vn.citynow.gatewayservice.constant;

public enum  ResponseMessage {
    SUCCESS("success");

    private String message;

    ResponseMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
