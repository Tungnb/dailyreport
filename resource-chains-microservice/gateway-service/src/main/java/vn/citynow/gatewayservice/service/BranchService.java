package vn.citynow.gatewayservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.citynow.gatewayservice.model.Branch;
import vn.citynow.gatewayservice.repository.*;

import java.util.List;

@Service
@Slf4j
public class BranchService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BranchRepository branchRepository;
    private final OrganizationRepository organizationRepository;
    private final UserBranchOrganizationService userBranchOrganizationService;

    public BranchService(UserRepository userRepository, RoleRepository roleRepository, BranchRepository branchRepository, OrganizationRepository organizationRepository, UserBranchOrganizationRepository userBranchOrganizationRepository, UserBranchOrganizationService userBranchOrganizationService) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.branchRepository = branchRepository;
        this.organizationRepository = organizationRepository;
        this.userBranchOrganizationService = userBranchOrganizationService;
    }

    public List<Branch> getByToken(String organization) {
        log.info("Get all branches by token");
        return userBranchOrganizationService.getBranch(organization);
    }

}
