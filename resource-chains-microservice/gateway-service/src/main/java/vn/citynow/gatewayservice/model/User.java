package vn.citynow.gatewayservice.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.time.Instant;
import java.util.*;

import static vn.citynow.gatewayservice.constant.RoleCode.SUPER_ADMIN;

@Entity
@Table(name = "Users", schema="platform")
@Getter
@Setter
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String username;

    private String email;

    private String password;

    private String avatar;

    private String firstName;

    private String lastName;

    private String nameHiragana;

    private String address;

    private String phoneNumber;

    private String permission;

    private String frontOfCard;

    private String backOfCard;

    private Date dateOfExpiration;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_id")
    private Status status;

    private Long openProjectId;

    private boolean delFlag = false;

    @Column(nullable = false, updatable = false)
    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

    @CreatedBy
    private String createdBy;

    @LastModifiedBy
    private String updatedBy;

    private Boolean isAccountNonExpired = true;

    private Boolean isAccountNonLocked = true;

    private Boolean isCredentialsNonExpired = true;

    private Boolean isEnabled = true;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_code")
    private Role role;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "user", orphanRemoval = true)
    private List<UserBranchOrganization> userBranchOrganizations = new ArrayList<>();


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (this.role != null && this.role.getCode().equals(SUPER_ADMIN)){
            Set<GrantedAuthority> authorities = new HashSet<>();
            authorities.add(new SimpleGrantedAuthority(SUPER_ADMIN));
            return authorities;
        }
        return Collections.emptySet();
    }

    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder().encode(password);
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }

    public User role(Role role) {
        if (role != null) this.role = role;
        return this;
    }

    public User email(String email) {
        if (email != null) this.email = email;
        return this;
    }

    public User firstName(String firstName) {
        if (firstName != null) this.firstName = firstName;
        return this;
    }

    public User lastName(String lastName) {
        if (lastName != null) this.lastName = lastName;
        return this;
    }

    public User nameHiragana(String nameHiragana) {
        if (nameHiragana != null) this.nameHiragana = nameHiragana;
        return this;
    }

    public User address(String address) {
        if (address != null) this.address = address;
        return this;
    }

    public User phoneNumber(String phoneNumber) {
        if (phoneNumber != null) this.phoneNumber = phoneNumber;
        return this;
    }

    public User permission(String permission) {
        if (permission != null) this.permission = permission;
        return this;
    }

    public User dateOfExpiration(Date dateOfExpiration) {
        if (dateOfExpiration != null) this.dateOfExpiration = dateOfExpiration;
        return this;
    }

    public User status(Status status) {
        if (status != null) this.status = status;
        return this;
    }

    public User password(String password) {
        if (password != null) this.password = new BCryptPasswordEncoder().encode(password);
        return this;
    }

    public User avatar(String avatar) {
        if (avatar != null) this.avatar = avatar;
        return this;
    }

    public User frontOfCard(String frontOfCard) {
        if (frontOfCard != null) this.frontOfCard = frontOfCard;
        return this;
    }

    public User backOfCard(String backOfCard) {
        if (backOfCard != null) this.backOfCard = backOfCard;
        return this;
    }

    public User addUserBranchOrganization(UserBranchOrganization userBranchOrganization) {
        if (this.userBranchOrganizations == null) this.userBranchOrganizations = new ArrayList<>();
        if (userBranchOrganization != null) {
            this.userBranchOrganizations.add(userBranchOrganization);
            userBranchOrganization.setUser(this);
        }
        return this;
    }

}
