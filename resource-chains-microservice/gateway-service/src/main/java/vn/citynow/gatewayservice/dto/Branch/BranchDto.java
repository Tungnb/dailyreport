package vn.citynow.gatewayservice.dto.Branch;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class BranchDto {

    Long id;

    @NotBlank
    @Pattern(regexp = "^[a-zA-Z_][a-zA-Z_0-9]*$")
    private String code;

    @NotBlank
    private String name;

}
