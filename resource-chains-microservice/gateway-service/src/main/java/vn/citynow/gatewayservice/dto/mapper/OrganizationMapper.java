package vn.citynow.gatewayservice.dto.mapper;

import org.mapstruct.Mapper;
import vn.citynow.gatewayservice.dto.Organization.OrganizationDto;
import vn.citynow.gatewayservice.model.Organization;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrganizationMapper {

    OrganizationDto toOrganizationDto(Organization organization);

    List<OrganizationDto> toOrganizationDtoList(List<Organization> organizationList);

}
