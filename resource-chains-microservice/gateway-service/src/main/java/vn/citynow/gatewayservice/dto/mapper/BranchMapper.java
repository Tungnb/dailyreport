package vn.citynow.gatewayservice.dto.mapper;

import org.mapstruct.Mapper;
import vn.citynow.gatewayservice.dto.Branch.BranchDto;
import vn.citynow.gatewayservice.model.Branch;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BranchMapper {

    BranchDto toBranchDto(Branch branch);

    List<BranchDto> toBranchDtoList(List<Branch> branchList);

}
