package vn.citynow.gatewayservice.config;

import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import vn.citynow.gatewayservice.dto.ApiResponse;
import vn.citynow.gatewayservice.repository.UserBranchOrganizationRepository;
import vn.citynow.gatewayservice.repository.UserRepository;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static vn.citynow.gatewayservice.constant.RoleCode.SUPER_ADMIN;


@Slf4j
public class CustomAuthenticationFilter implements Filter {

    private final UserBranchOrganizationRepository userBranchOrganizationRepository;
    private final UserRepository userRepository;

    public CustomAuthenticationFilter(UserBranchOrganizationRepository userBranchOrganizationRepository, UserRepository userRepository) {
        this.userBranchOrganizationRepository = userBranchOrganizationRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // NOPE
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request.getParameter("grant_type") != null && request.getParameter("grant_type").equals("password")) {
            String username = request.getParameter("username");
            String organization = request.getParameter("organization");

            log.info("Check if user " + username + " in organization " + organization);

            if (userRepository.existsByDelFlagFalseAndUsernameAndRoleCode(username, SUPER_ADMIN)) {
                chain.doFilter(request, response);
            } else if (organization == null
                    || !userBranchOrganizationRepository.existsByDelFlagFalseAndUserUsernameAndOrganizationCode(username, organization)) {
                ApiResponse apiResponse = new ApiResponse(HttpStatus.NOT_FOUND, "Cannot find user");
                ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_NOT_FOUND);
                ObjectMapper mapper = new ObjectMapper();
                response.getWriter().write(mapper.writeValueAsString(apiResponse));
            } else chain.doFilter(request, response);
        } else chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // NOPE
    }

}
