package vn.citynow.gatewayservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import vn.citynow.gatewayservice.api.Exception.ResourceNotFoundException;
import vn.citynow.gatewayservice.dto.mapper.UserMapper;
import vn.citynow.gatewayservice.model.User;
import vn.citynow.gatewayservice.repository.*;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final UserBranchOrganizationService userBranchOrganizationService;

    private final String uri = "http://0.0.0.0:8080/api/v3/users";

    @Autowired
    public UserService(UserRepository userRepository, UserBranchOrganizationService userBranchOrganizationService) {
        this.userRepository = userRepository;
        this.userBranchOrganizationService = userBranchOrganizationService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return userRepository
                .findByDelFlagFalseAndUsername(username)
                .filter(user -> user.getUsername().equals(username))
                .orElseThrow(() -> new ResourceNotFoundException("Cannot find user"));
    }

    private Specification<User> getUserSpecification(Long id, String search, String branch, String organization) {
        return (Specification<User>) (root, query, builder) -> {
            query.distinct(true);

            Join<Object, Object> userBranchOrganizations = root.join("userBranchOrganizations", JoinType.INNER);
            List<Predicate> predicateList = new ArrayList<>();

            if (id != null) {
                predicateList.add(builder.equal(root.get("id"), id));
            } else if (search != null) {
                String fixSearch = "%" + search
                        .replace("_", "\\_")
                        .replace("%", "\\%") + "%";
                List<Predicate> searchPredicateList = new ArrayList<>();
                searchPredicateList.add(builder.like(root.get("username"), fixSearch));
                searchPredicateList.add(builder.like(root.get("email"), fixSearch));
                searchPredicateList.add(builder.like(root.get("firstName"), fixSearch));
                searchPredicateList.add(builder.like(root.get("lastName"), fixSearch));
                searchPredicateList.add(builder.like(root.get("nameHiragana"), fixSearch));
                searchPredicateList.add(builder.like(root.get("address"), fixSearch));
                searchPredicateList.add(builder.like(root.get("phoneNumber"), fixSearch));
                predicateList.add(builder.or(searchPredicateList.toArray(new Predicate[0])));
            }
            if (branch != null) {
                predicateList.add(builder.equal(userBranchOrganizations.get("branch").get("code"), branch));
            }
            return builder.and(
                    builder.isFalse(root.get("delFlag")),
                    builder.isFalse(userBranchOrganizations.get("delFlag")),
                    builder.equal(userBranchOrganizations.get("organization").get("code"), organization),
                    builder.and(predicateList.toArray(new Predicate[0])));
        };
    }

    public Page<User> getAll(int page, int size, String direction, String sort, String search, String branch, String organization) {
        log.info("Get all users" + " with branch " + branch + " organization " + organization);
        userBranchOrganizationService.checkPermissionAndGetUser(branch, organization, "GET_ALL_USERS");

        Pageable pageable = PageRequest.of(page - 1, size,
                sort.equals("")
                        ? Sort.by(Sort.Direction.DESC, "createdAt")
                        : Sort.by(Sort.Direction.fromString(direction), sort));
        return userRepository.findAll(getUserSpecification(null, search, branch, organization), pageable)
                .map(user -> user.role(userBranchOrganizationService
                        .getUserBranchOrganization(user.getId(), branch, organization)
                        .getRole()));
    }

    public List<User> listAll(String search, String organization) {
        log.info("Get all usernames" + " with organization " + organization);
        userBranchOrganizationService.checkPermission(organization, "GET_ALL_USERS_IN_ORGANIZATION");

        search = search
                .replace("_", "\\_")
                .replace("%", "\\%");
        return userRepository.findAll(
                getUserSpecification(null, search, null, organization),
                Sort.by(Sort.Direction.ASC, "username"))
                .stream()
                .distinct()
                .collect(Collectors.toList());
    }

    public List<User> list(String search, String branch, String organization) {
        log.info("Get all usernames" + " with branch " + branch + " organization " + organization);
        userBranchOrganizationService.checkPermissionAndGetUser(branch, organization, "GET_ALL_USERS");

        search = search
                .replace("_", "\\_")
                .replace("%", "\\%");
        return userRepository.findAll(
                getUserSpecification(null, search, branch, organization),
                Sort.by(Sort.Direction.ASC, "username"));
    }

    public User getById(Long id, String branch, String organization) {
        log.info("Get user with id " + id
                + " with branch " + branch + " organization " + organization);
        userBranchOrganizationService.checkPermissionAndGetUser(branch, organization, "GET_USER_BY_ID");

        return userRepository
                .findOne(getUserSpecification(id, null, branch, organization))
                .orElseThrow(() -> new ResourceNotFoundException("Cannot find user"));
    }

    public User getByToken(String branch, String organization) {
        log.info("Get user with by token"
                + " with branch " + branch + " organization " + organization);

        return userBranchOrganizationService
                .checkPermissionAndGetUser(branch, organization, "GET_USER_BY_TOKEN");
    }

}
