package vn.citynow.gatewayservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import vn.citynow.gatewayservice.model.UserBranchOrganization;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserBranchOrganizationRepository extends JpaRepository<UserBranchOrganization, Long> {

    @Transactional(readOnly = true)
    Optional<UserBranchOrganization> findByDelFlagFalseAndUserIdAndBranchIdAndOrganizationCode(Long user_id, Long branch_id, String organization_code);

    @Transactional(readOnly = true)
    Optional<UserBranchOrganization> findByDelFlagFalseAndUserIdAndBranchCodeAndOrganizationCode(Long user_id, String branch_code, String organization_code);

    @Transactional(readOnly = true)
    Optional<UserBranchOrganization> findByDelFlagFalseAndUserUsernameAndBranchCodeAndOrganizationCode(String user_username, String branch_code, String organization_code);

    @Transactional(readOnly = true)
    boolean existsByUserId(Long user_id);

    List<UserBranchOrganization> findAllByDelFlagFalseAndUserUsernameAndOrganizationCode(String user_username, String organization_code);

    @Transactional(readOnly = true)
    boolean existsByDelFlagFalseAndUserIdAndBranchIdAndOrganizationCode(Long user_id, Long branch_id, String organization_code);

    @Transactional
    @Modifying
    @Query("update UserBranchOrganization ubo set ubo.delFlag = true where ubo.branch.id = ?1 and ubo.organization.code = ?2")
    int deleteUserBranchOrganization(Long branch_id, String organization_code);

    @Transactional(readOnly = true)
    boolean existsByDelFlagFalseAndUserUsernameAndOrganizationCode(String user_username, String organization_code);

}
