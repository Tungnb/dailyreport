package vn.citynow.gatewayservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.citynow.gatewayservice.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role,String> {

}
