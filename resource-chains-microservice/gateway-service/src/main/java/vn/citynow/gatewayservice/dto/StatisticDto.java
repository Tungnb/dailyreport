package vn.citynow.gatewayservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatisticDto {

    private Long userId;

    private String userName;

    private String firstName;

    private String lastName;

    private Instant startedDate;

    private Instant finishedDate;

    private LocalTime startedTime;

    private LocalTime finishedTime;

    private Integer totalTime;

    private Instant checkinTime;

    private Instant checkoutTime;

    private Integer workingTime;

    private Integer workingPercent;

}
