package vn.citynow.gatewayservice.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import vn.citynow.gatewayservice.dto.User.UserDto;
import vn.citynow.gatewayservice.dto.User.UserRequestDto;
import vn.citynow.gatewayservice.dto.User.UsernameDto;
import vn.citynow.gatewayservice.model.User;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    //UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "openProjectId", ignore = true)
    @Mapping(target = "userBranchOrganizations", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "frontOfCard", ignore = true)
    @Mapping(target = "backOfCard", ignore = true)
    @Mapping(target = "avatar", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "role", ignore = true)
    @Mapping(target = "delFlag", ignore = true)
    @Mapping(target = "isEnabled", ignore = true)
    @Mapping(target = "isCredentialsNonExpired", ignore = true)
    @Mapping(target = "isAccountNonLocked", ignore = true)
    @Mapping(target = "isAccountNonExpired", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "updatedBy", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "authorities", ignore = true)
    User toUser(UserRequestDto userRequestDto);

    @Mapping(target = "roleCode", source = "role.code")
    UserDto toUserDto(User user);

    UsernameDto toUsernameDto(User user);

    List<UserDto> toUserDtoList(List<User> users);

    List<UsernameDto> toUsernameDtoList(List<User> users);

}
