package vn.citynow.gatewayservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user_branch_organization", schema = "platform")
@Getter
@Setter
public class UserBranchOrganization {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_code", nullable = false)
    private Organization organization;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "branch_id", nullable = false)
    private Branch branch;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(optional = false)
    @JoinColumn(name = "role_code", nullable = false)
    private Role role;

    private boolean delFlag;

    public UserBranchOrganization organization(Organization organization) {
        this.organization = organization;
        return this;
    }

    public UserBranchOrganization branch(Branch branch) {
        this.branch = branch;
        return this;
    }

    public UserBranchOrganization user(User user) {
        this.user = user;
        return this;
    }

    public UserBranchOrganization role(Role role) {
        this.role = role;
        return this;
    }

    public UserBranchOrganization delete() {
        this.delFlag = true;
        return this;
    }

}
