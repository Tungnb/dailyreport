package vn.citynow.gatewayservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.citynow.gatewayservice.dto.mapper.StatusMapper;
import vn.citynow.gatewayservice.model.Status;
import vn.citynow.gatewayservice.repository.OrganizationRepository;
import vn.citynow.gatewayservice.repository.StatusRepository;
import vn.citynow.gatewayservice.repository.UserRepository;

import java.util.List;

@Service
@Slf4j
public class StatusService {

    private final StatusRepository statusRepository;
    private final UserBranchOrganizationService userBranchOrganizationService;

    public StatusService(StatusRepository statusRepository, UserBranchOrganizationService userBranchOrganizationService) {
        this.statusRepository = statusRepository;
        this.userBranchOrganizationService = userBranchOrganizationService;
    }

    public List<Status> getAll(String organization) {
        log.info("Get all status" + " with organization " + organization);
        userBranchOrganizationService.checkPermission(organization, "GET_ALL_STATUS");

        return statusRepository.findAllByDelFlagFalseAndOrganizationCode(organization);
    }

}
