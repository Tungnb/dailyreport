package vn.citynow.gatewayservice.constant;

public class DefaultPageable {

    public static final String PAGE = "1";
    public static final String SIZE = "100";

}
